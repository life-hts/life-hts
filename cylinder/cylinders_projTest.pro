Include "cylinders_data.pro";
Include "../lib/commonInformation.pro";

Group {
    // Preset choice of formulation
    DefineConstant[preset = {4, Highlight "Blue",
      Choices{
        1="h-formulation",
        3="a-formulation (small steps)",
        4="h-a-formulation"},
      Name "Input/5Method/0Preset formulation" },
      expMode = {0, Choices{0,1}, Name "Input/5Method/1Allow changes?"}];
    // Output choice
    DefineConstant[onelabInterface = {0, Choices{0,1}, Name "Input/3Problem/2Get solution during simulation?"}]; // Set to 0 for launching in terminal (faster)
    realTimeInfo = 1;
    realTimeSolution = onelabInterface;

    alt_formulation = 0; // Experimental for a-formulation (normally ok with h-form.)

// ------- PROBLEM DEFINITION -------
    // Test name - for output files
    name = "cylinder";
    // (directory name for .txt files, not .pos files)
    testname = "cylinders_model";
    // Dimension of the problem
    Dim = 2;
    // Axisymmetry of the problem, 0: no, 1: yes
    Axisymmetry = 0;

    // ------- WEAK FORMULATION -------
    // Choice of the formulation
    formulation = (preset==1) ? h_formulation : ((preset == 3) ? a_formulation : coupled_formulation);

    // ------- Definition of the physical regions -------
    // What cylinders are made up of? 0: none (air), 1: super, 2: ferro, 3: both
    MaterialsType = 3;
    // Filling the regions
    Air = Region[ AIR ];
    Air += Region[ AIR_OUT ];
    If(MaterialsType == 0)
        Air += Region[ SUPER ];
        Air += Region[ FERRO ];
    ElseIf(MaterialsType == 1)
        Super += Region[ SUPER ];
        BndOmegaC += Region[ BND_OMEGA_C ];
        IsThereSuper = 1;
        Air += Region[ FERRO ];
    ElseIf(MaterialsType == 2)
        Air += Region[ SUPER ];
        Ferro = Region[ FERRO ];
        IsThereFerro = 1;
    ElseIf(MaterialsType == 3)
        Super += Region[ SUPER ];
        BndOmegaC += Region[ BND_OMEGA_C ];
        Ferro += Region[ FERRO ];
        IsThereSuper = 1;
        IsThereFerro = 1;
    EndIf

    // Fill the regions for formulation
    MagnAnhyDomain = Region[ {Ferro} ];
    MagnLinDomain = Region[ {Air, Super, Copper} ];
    NonLinOmegaC = Region[ {Super} ];
    LinOmegaC = Region[ {Copper} ];
    OmegaC = Region[ {LinOmegaC, NonLinOmegaC} ];
    OmegaCC = Region[ {Air, Ferro} ];
    Omega = Region[ {OmegaC, OmegaCC} ];

    // Boundaries for BC
    CentralPoint = Region[ ARBITRARY_POINT ];
    SurfOut = Region[ SURF_OUT ];
    SurfSym = Region[ SURF_SYM ];
    SurfSym += Region[ SURF_SYM_MAT ];
    If(formulation == h_formulation)
        Gamma_h = Region[{SurfOut}]; // Essential BC
        Gamma_e = Region[{SurfSym}]; // Natural BC
    ElseIf(formulation == a_formulation)
        Gamma_h = Region[{}]; // Natural BC
        Gamma_e = Region[{SurfOut, SurfSym}]; // Essential BC
    ElseIf(formulation == coupled_formulation)
        Gamma_h = Region[{}];
        Gamma_e = Region[{SurfOut, SurfSym}];
    EndIf
    GammaAll = Region[ {Gamma_h, Gamma_e} ];

}

Function{
    // ------- PARAMETERS -------
    // Superconductor parameters
    DefineConstant [jc = {3e8, Name "Input/3Material Properties/2jc (Am⁻²)"}]; // Critical current density [A/m2]
    DefineConstant [n = {40, Name "Input/3Material Properties/1n (-)"}]; // Superconductor exponent (n) value [-]
    // Ferromagnetic material parameters
    DefineConstant [mur0 = {1700.0, Name "Input/3Material Properties/3mur at low fields (-)"}]; // Relative permeability at low fields [-]
    DefineConstant [m0 = {1.04e6, Name "Input/3Material Properties/4Saturation field (Am^-1)"}]; // Magnetic field at saturation [A/m]
    // Excitation - Source field or imposed current intensty
    // 0: sine, 1: triangle, 2: up-down-pause, 3: step, 4: up-pause-down
    DefineConstant [Flag_Source = {4, Highlight "yellow", Choices{
        0="Sine",
        1="Triangle",
        4="Up-down"}, Name "Input/4Source/0Source field type" }];
    DefineConstant [f = {0.1, Visible (Flag_Source ==0), Name "Input/4Source/1Frequency (Hz)"}]; // Frequency of imposed current intensity [Hz]
    DefineConstant [bmax = {1, Name "Input/4Source/2Field amplitude (T)"}]; // Maximum applied magnetic induction [T]
    DefineConstant [partLength = {5, Visible (Flag_Source != 0), Name "Input/4Source/1Ramp duration (s)"}];
    DefineConstant [timeStart = 0]; // Initial time [s]
    DefineConstant [timeFinal = (Flag_Source == 0) ? 5/(4*f) : ((Flag_Source == 1) ? 5*partLength : 2*partLength)]; // Final time for source definition [s]
    DefineConstant [timeFinalSimu = meshMult*timeFinal/120]; // Final time of simulation [s]

    // ------- NUMERICAL PARAMETERS -------
    DefineConstant [dt = {meshMult*timeFinal/120, Highlight "LightBlue",
        ReadOnly !expMode, Name "Input/5Method/Time step (s)"}]; // Time step (initial if adaptive)[s]
    DefineConstant [writeInterval = dt]; // Time interval between two successive output file saves [s]
    DefineConstant [dt_max = dt]; // Maximum allowed time step [s]
    DefineConstant [tol_energy = {(preset == 3) ? 1e-4 : 1e-6, Highlight "LightBlue",
        ReadOnly !expMode, Name "Input/5Method/Relative tolerance (-)"}]; // Relative tolerance on the energy estimates
    DefineConstant [iter_max = {(preset==3) ? 600 : 50, Highlight "LightBlue",
        ReadOnly !expMode, Name "Input/5Method/Max number of iteration (-)"}]; // Maximum number of nonlinear iterations
    DefineConstant [extrapolationOrder = (preset==3) ? 2 : 1]; // Extrapolation order
    // Control points
    controlPoint1 = {1e-5,0, 0}; // CP1
    controlPoint2 = {W/2-1e-5, 0, 0}; // CP2
    controlPoint3 = {0, H_super/2+2e-3, 0}; // CP3
    controlPoint4 = {W/2, H_super/2+2e-3, 0}; // CP4
    savedPoints = 2000; // Resolution of the line saving postprocessing


    GmshRead["Last_computed_h.pos",1];
    h_fromFile[] = VectorField[XYZ[]]{1};
    GmshRead["Last_computed_a.pos",2];
    a_fromFile[] = VectorField[XYZ[]]{2};
    //GmshRead["Last_computed_h_forh.pos",3];
    //hh_fromFile[] = VectorField[XYZ[]]{3};

}

Include "../lib/lawsAndFunctions.pro";

Function{
    // Direction of applied field
    directionApplied[] = Vector[0., 1., 0.]; // Only choice for axi
    hmax = bmax / mu0;
    If(Flag_Source == 0)
        // Sine source field
        controlTimeInstants = {timeFinalSimu, 1/(2*f), 1/f, 3/(2*f), 2*timeFinal};
        hsVal[] = hmax * Sin[2.0 * Pi * f * $Time];
    ElseIf(Flag_Source == 1)
        // Triangle source field (5/4 of a complete cycle)
        controlTimeInstants = {timeFinal, timeFinal/5.0, 3.0*timeFinal/5.0, timeFinal};
        rate = hmax * 5.0 / timeFinal;
        hsVal[] = (($Time < timeFinal/5.0) ? $Time * rate :
                    ($Time >= timeFinal/5.0 && $Time < 3.0*timeFinal/5.0) ?
                    hmax - ($Time - timeFinal/5.0) * rate :
                    - hmax + ($Time - 3.0*timeFinal/5.0) * rate);
    ElseIf(Flag_Source == 4)
        // Up-pause-down
        controlTimeInstants = {timeFinal/2.0, timeFinal};
        rate = hmax * 2.0 / timeFinal;
        hsVal[] = 0*(($Time < timeFinal/2.0) ? $Time * rate : hmax - ($Time - timeFinal/2.0) * rate);
    EndIf
}


// Only external field is implemented
Constraint {
    { Name a ;
        Case {
            // Square because axisymmetry and circulation on perpendicular edge
            {Region Gamma_e ; Value -X[] * mu0 ; TimeFunction hsVal[] ;}
        }
    }
    { Name a2 ;
        Case {
            {Region Gamma_e ; Value 0.0 ;} // Second-order hierarchical elements for coupled formulation
        }
    }
    { Name phi ;
        Case {
            {Region Gamma_h ; Value XYZ[]*directionApplied[] ; TimeFunction hsVal[] ;}
        }
    }
    { Name j ;
        Case {
            {Region SurfSym ; Value 0.0 ;}
        }
    }
    { Name Voltage ;
        Case {
            If(formulation == h_formulation || formulation == coupled_formulation)
            // No cut is defined in this geometry (no applied current) -> No associated constraint
            Else
                // a-formulation and BF_RegionZ
                { Region OmegaC; Value 0.0; }
            EndIf
        }
    }
}

Include "../lib/jac_int.pro";
Include "../lib/formulations.pro";
Include "../lib/resolution.pro";

PostOperation {
    // Runtime output for graph plot
    { Name Info;
        If(formulation == h_formulation)
            NameOfPostProcessing MagDyn_htot ;
        ElseIf(formulation == a_formulation)
            NameOfPostProcessing MagDyn_avtot ;
        ElseIf(formulation == coupled_formulation)
            NameOfPostProcessing MagDyn_coupled ;
        EndIf
        Operation{
            Print[ time[OmegaC], OnRegion OmegaC, LastTimeStepOnly, Format Table, SendToServer "Output/0Time [s]"] ;
            Print[ bsVal[OmegaC], OnRegion OmegaC, LastTimeStepOnly, Format Table, SendToServer "Output/1Applied field [T]"] ;
            Print[ m_avg_y_tesla[OmegaC], OnGlobal, LastTimeStepOnly, Format Table, SendToServer "Output/2Avg. magnetization [T]"] ;
            Print[ dissPower[OmegaC], OnGlobal, LastTimeStepOnly, Format Table, SendToServer "Output/2Joule loss [W]"] ;
        }
    }
    { Name MagDyn;LastTimeStepOnly realTimeSolution ;
        If(formulation == h_formulation)
            NameOfPostProcessing MagDyn_htot;
        ElseIf(formulation == a_formulation)
            NameOfPostProcessing MagDyn_avtot;
        ElseIf(formulation == coupled_formulation)
            NameOfPostProcessing MagDyn_coupled;
        EndIf
        Operation {
            If(economPos == 0)
                If(formulation == h_formulation)
                    Print[ phi, OnElementsOf OmegaCC , File "res/phi.pos", Name "phi [A]" ];
                    Print[ mur, OnElementsOf OmegaCC , File "res/mur.pos", Name "mur [-]" ];
                ElseIf(formulation == a_formulation)
                    Print[ a, OnElementsOf Omega , File "res/a.pos", Name "a [Tm]" ];
                    Print[ ur, OnElementsOf OmegaC , File "res/ur.pos", Name "ur [V/m]" ];
                    Print[ mur, OnElementsOf OmegaCC , File "res/mur.pos", Name "mur [-]" ];
                ElseIf(formulation == coupled_formulation)
                    //Print[ phi, OnElementsOf BndOmegaC , File "res/phi.pos", Name "phi [A]" ];
                    Print[ a, OnElementsOf OmegaCC , File "res/a.pos", Name "a [Tm]" ];
                    Print[ mur, OnElementsOf OmegaCC , File "res/mur.pos", Name "mur [-]" ];
                EndIf
                Print[ j, OnElementsOf OmegaC , File "res/j.pos", Name "j [A/m2]" ];
                Print[ jz, OnElementsOf OmegaC , File "res/jz.pos", Name "j [A/m2]" ];
                Print[ e, OnElementsOf OmegaC , File "res/e.pos", Name "e [V/m]" ];
                Print[ h, OnElementsOf Omega , File "res/h.pos", Name "h [A/m]" ];
                Print[ b, OnElementsOf Omega , File "res/b.pos", Name "b [T]" ];
                If(alt_formulation && formulation == h_formulation)
                    Print[ b_alt, OnElementsOf OmegaCC , File "res/b_alt.pos", Name "b_alt [T]" ];
                    Print[ h_alt, OnElementsOf MagnAnhyDomain , File "res/h_alt.pos", Name "h_alt [T]" ];
                ElseIf(alt_formulation && formulation == a_formulation)
                    Print[ j_alt, OnElementsOf OmegaC , File "res/j_alt.pos", Name "j_alt [A/m2]" ];
                EndIf
            EndIf
            Print[ j, OnLine{{List[controlPoint1]}{List[controlPoint2]}} {savedPoints},
                Format TimeTable, File outputCurrent];
            Print[ b, OnLine{{List[controlPoint1]}{List[controlPoint2]}} {savedPoints},
                Format TimeTable, File outputMagInduction1];
            Print[ b, OnLine{{List[controlPoint3]}{List[controlPoint4]}} {savedPoints},
                Format TimeTable, File outputMagInduction2];
            Print[ b, OnPlane{{List[controlPoint1]}{List[controlPoint2]}{List[controlPoint3]}} {100,50},
                File "res/b_onPlane.pos", Format Gmsh];
            Print[ hsVal[Omega], OnRegion Omega, Format TimeTable, File outputAppliedField];


            Print[ h, OnElementsOf Omega_h, File "Last_computed_h.pos", Format Gmsh,
                OverrideTimeStepValue 0, LastTimeStepOnly, SendToServer "No"] ;
            Print[ a, OnElementsOf Omega_a, File "Last_computed_a_vol.pos",Format Gmsh,
              OverrideTimeStepValue 0, LastTimeStepOnly, SendToServer "No"] ;
            Print[ a_bnd, OnElementsOf BndOmega_ha, File "Last_computed_a.pos",Format Gmsh,
                OverrideTimeStepValue 0, LastTimeStepOnly, SendToServer "No"] ;
        }
    }
}

DefineConstant[
  R_ = {"MagDyn", Name "GetDP/1ResolutionChoices", Visible 0},
  C_ = {"-solve -pos -bin -v 3 -v2", Name "GetDP/9ComputeCommand", Visible 0},
  P_ = { "MagDyn", Name "GetDP/2PostOperationChoices", Visible 0}
];
